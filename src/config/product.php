<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Product Reference
    |--------------------------------------------------------------------------
    |
    | This value is used to activate the reference column of a product. 
    | If disabled, the product will have no reference except 
    | the generated database identifier
    |
    */
    'reference' => env('PRODUCT_REF', true),
];
