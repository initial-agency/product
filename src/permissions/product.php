<?php
/*
|--------------------------------------------------------------------------
| Product Permissions
|--------------------------------------------------------------------------
|
| This is an example file.
|
*/
return [
    'product' => [
        'create' => 'Create a product',
        'read'=> 'Read a product',
        'update' => 'Update a product',
        'delete' => 'Delete a product',
    ],
];
