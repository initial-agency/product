<?php

namespace Initial\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Initial\Product\Traits\Uuids;

class Product extends Model
{
    use Uuids;
    /**
    * Indicates if the IDs are auto-incrementing.
    *
    * @var bool
    */
    public $incrementing = false;
}
