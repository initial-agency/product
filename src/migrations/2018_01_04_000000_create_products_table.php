<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->uuid('id');
            $table->text('desc')->nullable();
            $table->integer('price')->nullable();
            $table->integer('stock')->nullable();
            if(config('product.reference')) {
                $table->string('reference', 48)->nullable();                
            }
            $table->string('title', 64);
            $table->timestamps();
            
            $table->primary('id');
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
