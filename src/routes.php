<?php

Route::group([
    'prefix' => 'admin',
    'middleware' => ['web','admin'],
    'namespace' => 'Initial\Product\Controllers'],
    function() {
        Route::get('/products','ProductController@index');
    }
);
