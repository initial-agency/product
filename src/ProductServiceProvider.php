<?php

namespace Initial\Product;

use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{   
    /**
    * Bootstrap the application events.
    *
    * @return void
    */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        // Publish migrations files
        $this->publishes([
            __DIR__ . '/migrations/' => database_path('/migrations')
        ], 'migrations');
        //Publish config file
        $this->publishes([
            __DIR__ . '/config/product.php' => config_path('product.php')
        ], 'config');
        //Publish permission files
        $this->publishes([
            __DIR__ . '/permissions/product.php' => core_path('permissions/product.php')
        ],'permission');
        // Load views
        $this->loadViewsFrom(__DIR__.'/resources/views/', 'products');

        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }
    
    /**
    * Register the service provider.
    *
    * @return void
    */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/product.php', 'product'
        );
    }
}
